/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kittaphat.oop_fin;

import java.io.Serializable;

/**
 *
 * @author kitta
 */
public class OrderList implements Serializable{
    private String customerList;
    private String choice;
    private String foodOrderList;
    
    public OrderList(String customerList, String choice, String foodOrderList) {
        this.customerList = customerList;
        this.choice = choice;
        this.foodOrderList = foodOrderList;
    }

    public String getCustomerList() {
        return customerList;
    }

    public void setCustomerList(String customerList) {
        this.customerList = customerList;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public String getFoodOrderList() {
        return foodOrderList;
    }

    public void setFoodOrderList(String foodOrderList) {
        this.foodOrderList = foodOrderList;
    }

    @Override
    public String toString() {
        return "OrderList{" + "customerList=" + customerList + ", choice=" + choice + ", foodOrderList=" + foodOrderList + '}';
    }
    
}
